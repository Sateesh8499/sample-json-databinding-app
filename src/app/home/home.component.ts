import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private postdata : DataService) { }

    posts : Object;

  ngOnInit() {
      this.postdata.getPosts().subscribe(postdata =>{
          this.posts = postdata;
          console.log(this.posts);
      })
  }

}
