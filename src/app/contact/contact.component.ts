import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  constructor(private albumsdata: DataService) { }

album : Object;

  ngOnInit() {
      this.albumsdata.getAlbums().subscribe(albumsdata =>{
          this.album = albumsdata;
          console.log(this.album)
      })
  }

}
