import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor() { }

contactInfo : any;

  ngOnInit() {
      this.contactInfo = {
          name : "John Doe",
          age : 23,
          phone : "1520 154 154",
          email : "johndoe143@gmail.com"
      }
  }

}
